//
//  Recipe.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import Foundation

struct Recipe: Identifiable {
    var id: UUID = UUID()
    var name: String
    var description: String
    var instructions: String
    var cookingTime: String
    var type: Int
}
