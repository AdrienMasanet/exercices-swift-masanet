//
//  AddButtonView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct AddButtonView: View {
    
    @Binding var addingRecipeFormIsOpen: Bool
    
    var body: some View {
        Button(action: {
            addingRecipeFormIsOpen.toggle()
        }) {
            Label("Rajouter une recette", systemImage: "plus.app")
        }
        .padding(5)
        .foregroundColor(Color.white)
        .background(Color.orange)
        .cornerRadius(5)
    }
}
