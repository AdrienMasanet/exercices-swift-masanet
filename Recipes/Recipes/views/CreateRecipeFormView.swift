//
//  CreateRecipeFormView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct CreateRecipeFormView: View {
    var body: some View {
        Text("Formulaire de création de recette")
    }
}

struct CreateRecipeFormView_Previews: PreviewProvider {
    static var previews: some View {
        CreateRecipeFormView()
    }
}
