//
//  RecipeListElementView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct RecipeListElementView: View {
    
    var recipe: Recipe
    
    var body: some View {
        NavigationLink(destination: RecipeDetailView(recipe: recipe)) {
            HStack(alignment: .top) {
                IconDependingOnType()
                
                Text(recipe.name)
                    .foregroundColor(.white)
                    .font(.title2)
            }
            .cornerRadius(5)
        }
    }
    
    func IconDependingOnType() -> AnyView? {
        switch recipe.type {
        case 0:
            return AnyView(
                Image("entry_white")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 30, height: 30, alignment: .center)
                    .padding(.trailing, 5)
            )
        case 1:
            return AnyView(
                Image("maindish_white")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 30, height: 30, alignment: .center)
                    .padding(.trailing, 5)
            )
        case 2:
            return AnyView(
                Image("dessert_white")
                    .resizable()
                    .foregroundColor(.white)
                    .frame(width: 30, height: 30, alignment: .center)
                    .padding(.trailing, 5)
            )
        default:
            return nil
        }
    }
}

struct RecipeListElementView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeListElementView(recipe: recipes[2])
    }
}
