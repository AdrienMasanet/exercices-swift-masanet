//
//  RecipeDetailView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct RecipeDetailView: View {
    
    var recipe: Recipe
    
    var body: some View {
        VStack {
            
            Spacer()
            
            VStack {
                Text(recipe.name)
                    .font(.largeTitle)
                    .multilineTextAlignment(.center)
                Text(recipe.cookingTime)
                    .multilineTextAlignment(.center)
            }
            
            Spacer()
            
            Text(recipe.description)
                .italic()
                .font(.title3)
                .multilineTextAlignment(.center)
            Spacer()
            Text(recipe.instructions)
                .multilineTextAlignment(.center)
            Spacer()
            
        }
        .frame(width: 350)
    }
}

struct RecipeDetailView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeDetailView(recipe: recipes[0])
    }
}
