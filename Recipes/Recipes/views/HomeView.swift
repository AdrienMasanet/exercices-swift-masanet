//
//  ContentView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct HomeView: View {
    
    @State var addingRecipeFormIsOpen: Bool = false
    
    var body: some View {
        VStack {            
            AddButtonView(addingRecipeFormIsOpen: $addingRecipeFormIsOpen)
                .padding(.top, 50)
            
            TabView {
                NavigationView {
                    RecipesListView(recipeType: 0)
                        .navigationTitle("Entrées")
                }
                .tabItem {
                    TabItemView(image: "entry_darkbrown", text: "Entrées")
                }
                .tag(0)
                
                NavigationView {
                    RecipesListView(recipeType: 1)
                        .navigationTitle("Plats")
                }
                .tabItem {
                    TabItemView(image: "maindish_darkbrown", text: "Plats")
                }
                .tag(1)
                
                NavigationView {
                    RecipesListView(recipeType: 2)
                        .navigationTitle("Desserts")
                }
                .tabItem {
                    TabItemView(image: "dessert_darkbrown", text: "Desserts")
                }
                .tag(2)
                
                NavigationView {
                    RecipesListView(recipeType: 3)
                        .navigationTitle("Toutes les recettes")
                }
                .tabItem {
                    TabItemView(image: "all_darkbrown", text: "Tout")
                }
                .tag(3)
                
            }
        }
        .sheet(isPresented: $addingRecipeFormIsOpen) {
            CreateRecipeFormView()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .previewInterfaceOrientation(.portrait)
    }
}
