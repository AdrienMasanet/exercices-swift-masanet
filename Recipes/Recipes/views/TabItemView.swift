//
//  TabItemView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct TabItemView: View {
    
    var image: String
    var text: String
    
    var body: some View {
            VStack {
                Image(image)
                Text(text)
                    .foregroundColor(.white)
                    .padding(0)
            }
            .background(.orange)
    }
}

struct TabItemView_Previews: PreviewProvider {
    static var previews: some View {
        TabItemView(image: "all", text: "Tout")
    }
}
