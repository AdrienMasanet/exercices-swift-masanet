//
//  EntriesListView.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI

struct RecipesListView: View {
    
    var recipeType: Int
    
    var body: some View {
        VStack {
            List {
                
                ForEach(recipes) { recipe in
                    RecipeListElementView(recipe: recipe)
                }
                .listRowBackground(Color.orange)
                
            }
            .padding(.top, 12.5)
        }
    }
}

struct RecipesListView_Previews: PreviewProvider {
    static var previews: some View {
        RecipesListView(recipeType: 0)
    }
}
