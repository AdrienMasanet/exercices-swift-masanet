//
//  RecipesData.swift
//  Recipes
//
//  Created by Adrien Masanet on 10/06/2022.
//

var recipes = [
    Recipe(name: "Gratin de pommes de terre", description: "Un délicieux gratin permettant de se régaler", instructions: "Cuisiner les pommes de terre, mettre du lait et du poivre", cookingTime: "2 heures", type: 1),
    Recipe(name: "Gigot d'agneau", description: "Un bon gros gigot d'agneau pas piqué des hantetons, vous allez vous régaler wolag", instructions: "Découper une bonne grosse tranche bien grasse là et la jeter dans la poële", cookingTime: "3 heures", type: 1),
    Recipe(name: "Tarte aux abricots", description: "La même tarte que celle que fait mamie", instructions: "Prendre des abricots au marché (ils sont meilleurs qu'à Carrefour) puis les couper en deux, tartiner la pâte de compote d'abricot et le reste c'est un secret, cette recette est réservée aux mamies de toutes façons", cookingTime: "30 minutes", type: 2),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0),
    Recipe(name: "Salade Cesar", description: "Une salade à la fois healthy et dèlicieuse", instructions: "Mettre un max de salade, puis les noix, puis les croutons, puis le maïs, puis les ptits bouts de fromage trop bons, puis de l'huile de noix, puis les vraies noix, puis les émincés de poulet cuits", cookingTime: "10 minutes", type: 0)
]
