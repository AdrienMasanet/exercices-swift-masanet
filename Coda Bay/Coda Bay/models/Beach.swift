//
//  Beach.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import Foundation

struct Beach: Identifiable {
    var id = UUID()
    var name: String
    var image: String
    var country: String
    var rating: Int
    var description: String
}
