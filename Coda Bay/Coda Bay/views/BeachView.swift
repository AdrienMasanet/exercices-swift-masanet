//
//  BeachView.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct BeachView: View {
    
    let beach: Beach
    
    var body: some View {
        VStack {
            Text(beach.name)
                .font(.custom("PingFang TC Ultralight", size: 40))
                .fontWeight(.light)
                .foregroundColor(Color.white)
                .shadow(color: .black, radius: 0, x: 1, y: 1)
                .padding(.top, 70)
            
            
            Image(beach.image)
                .resizable()
                .cornerRadius(10)
                .shadow(color: .secondary, radius: 10, x: 2, y: 2)
                .frame(width: 300, height: 300, alignment: .center)
            
            HStack {
                ForEach(0..<5) { index in
                    Image(systemName: (self.beach.rating > index ? "star.fill" : "star"))
                }
            }
            .padding()
            .background(Color.white)
            .cornerRadius(20)
            .shadow(color: .secondary, radius: 2, x: 0, y: 3)
            .offset(y: -30)
            
            ScrollView {
                Text(beach.description)
                    .font(.custom("PingFang TC Ultralight", size: 20))
                    .frame(width: 350, height: .infinity, alignment: .leading)
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(Color.white)
                    .shadow(radius: 15)
            }
        }
        .aspectRatio(contentMode: .fill)
        .background(
            Image(beach.image)
                .resizable()
                .blur(radius: 7)
                .opacity(0.7)
                .aspectRatio( contentMode: .fill)
                .edgesIgnoringSafeArea(.all)
        )
    }
}

struct BeachView_Previews: PreviewProvider {
    static var previews: some View {
        BeachView(beach: placeHolderBeach)
    }
}
