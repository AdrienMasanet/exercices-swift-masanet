//
//  BeachListElement.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct BeachListElement: View {
    
    var beach: Beach
    
    var body: some View {
        NavigationLink(destination: BeachView(beach: beach)) {
            HStack {
                Image(beach.image)
                    .resizable()
                    .frame(width: 50, height: 50, alignment: .center)
                    .clipShape(Circle())
                    .shadow(color: .gray, radius: 2, x: 1, y: 1)
                    .padding(.trailing,5)
                Text(beach.name)
            }
        }
    }
}

struct BeachListElement_Previews: PreviewProvider {
    static var previews: some View {
        BeachListElement(beach: placeHolderBeach)
    }
}
