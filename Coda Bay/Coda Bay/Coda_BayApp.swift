//
//  Coda_BayApp.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

@main
struct Coda_BayApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
