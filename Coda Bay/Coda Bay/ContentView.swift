//
//  ContentView.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct ContentView: View {
    
    var beaches = BeachGetter().getBeaches()
    
    var body: some View {
        NavigationView {
            VStack() {
                Text("Coda Bay")
                    .padding()
                    .font(.custom("PingFang TC Ultralight", size: 50))
                    .foregroundColor(.white)
                    .shadow(color: .black, radius: 0, x: 2, y: 2)
                
                Spacer()
                
                List {
                    ForEach(beaches) { beach in
                        BeachListElement(beach: beach)
                    }
                }
            }
            .frame(maxWidth: .infinity)
            .background(.cyan)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
