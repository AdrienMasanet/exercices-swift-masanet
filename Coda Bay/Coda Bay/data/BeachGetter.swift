//
//  BeachGetter.swift
//  Coda Bay
//
//  Created by Adrien Masanet on 08/06/2022.
//

import Foundation

var placeHolderBeach = Beach(name: "La Ciotat", image: "ciotat", country: "France", rating: 4, description: "La baie de La Ciotat en méditerrannée se trouve à l'extrémité Est du parc naturel des calanques de Marseille et à seulement quelques kilomètres à l'Ouest de la réserve de Porquerolles.")

class BeachGetter {
    
    func getBeaches() -> [Beach] {
        let beaches: [Beach] = [
            Beach(name: "La Ciotat", image: "ciotat", country: "France", rating: 4, description: "La baie de La Ciotat en méditerrannée se trouve à l'extrémité Est du parc naturel des calanques de Marseille et à seulement quelques kilomètres à l'Ouest de la réserve de Porquerolles."),
            Beach(name: "Baie d'Along", image: "along", country: "Vietnam", rating: 5, description: "Signifie la descente du dragon en Vietnamien. Elle s'étend sur plus de 43 mille hectares et comprend presque 2000 iles. Ele fait partie depuis 2011 des 7 nouvelles merveilles de la nature"),
            Beach(name: "Cambodia", image: "cambodia", country: "Cambodge", rating: 3, description: "Longue de 4440 Kms, elle est d'une diversité incroyable alternant entre mangrove, recifs coraliens et sable blanc. La puissance des moussons dans cette partie du globe fait changer la baie d'aspect en fonction des saisons."),
            Beach(name: "Diego Suarez Beach", image: "diego", country: "Madagascar", rating: 4, description: "La baie de Diego Suarez est située à l’extrême nord de Madagascar et elle tient son nom de deux navigateurs portugais, Diego Diaz et l’amiral Suarez (XVI siècle). Elle est composée de quatre baies principales dont l’une a un îlot nommé le « Pain de sucre », considéré localement comme un lieu sacré."),
            Beach(name: "Matsushima", image: "matsushima", country: "Japon", rating: 4, description: "La baie de Matsushima est un site célèbre depuis l’antiquité, mentionné dans la poésie japonaise comme un lieu historique plein de romantisme et d’élégance. Les vues des 260 îles de pin de la baie de Matsushima restent pratiquement inchangées depuis quatre cents ans."),
            Beach(name: "Mont St Michel", image: "michel", country: "France", rating: 3, description: "Située en France, la baie du mont Saint-Michel est une baie située entre la Bretagne (au sud-ouest) et la péninsule normande du Cotentin (au sud et à l’est). La baie est inscrite au patrimoine mondial de l’UNESCO. Cette petite île granitique avec 950 m de circonférence et 80 m d’altitude attire l’homme depuis la pré-histoire. Derrière ses remparts, on retrouve un petit village, dont le mont est la couronne, et les magnifiques bâtiments de l’abbaye bénédictine."),
            Beach(name: "Mindelo", image: "mindelo", country: "Cap Vert", rating: 3, description: "Mindelo respire une atmosphère unique et un air cosmopolite. Cette atmosphère unique est inséparable de la chaleur et de l’hospitalité de ses habitants. Leur goût pour le côté amusant de la vie fait toute la différence. Certains disent qu’ici est née la morabeza (un mot indigène qui exprime l’accueil chaleureux)"),
            Beach(name: "Morbihan", image: "morbihan", country: "France", rating: 4, description: "Le golfe du Morbihan (« Mor-bihan » signifiant « petite mer » en breton) correspond à une dépression côtière, relié à l’océan par un étroit passage (0,9 km), parsemé de nombreuses îles. À chaque marée, environ 400 millions de m3 d’eau de mer entrent et sortent du golfe, créant de forts courants sur un fond plat. Ces marées contribuent à la création d’un écosystème maritime d’une grande biodiversité."),
            Beach(name: "Pemba Beach", image: "pemba", country: "Mozambique", rating: 4, description: "Pemba est une ville portuaire au Mozambique. C’est la capitale de la province de Cabo Delgado, en grande partie entourée par la péninsule de Pemba. La ville a été fondée par la compagnie Niassa en 1904 sous le nom de Porto Amélia (du nom de la reine du Portugal) à l’extrémité sud-ouest de la péninsule et s’est développée autour du port. La ville est renommée pour son architecture coloniale portugaise. Elle a été rebaptisée Pemba en 1975, avec la décolonisation des Portugais."),
            Beach(name: "San Francisco", image: "sanfran", country: "USA", rating: 4, description: "La baie de San Francisco se situe sur la côte nord de la Californie. Il s’agit à la fois d’une baie et d’un système estuarien peu profond qui s’étend sur environ 1 144 km². L’estuaire est situé au nord dans la baie de San Pablo, la baie de Suisun et le delta. L’estuaire et le delta drainent environ quarante pour cent de la superficie totale de la Californie, fournissant de l’eau à 22 millions de Californiens et irriguant 4,5 millions d’acres (1,82 million d’hectares) de terres agricoles.")
        ]
        
        return beaches
    }
    
}
