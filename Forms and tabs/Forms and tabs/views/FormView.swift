//
//  FormView.swift
//  Forms and tabs
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct FormView: View {
    @State var textWritten : String = ""
    
    @State var textSecure : String = ""
    
    @State var toggle : Bool = false
    
    @State var number : Int = 1
    
    @State var percent : Float = 50
    
    var fruits = ["poire", "pomme", "banane","cerise","fraise"]
    @State var indexSelectedFruit : Int = 0
    
    @State var date : Date = Date()
    var dateMin = Date(timeIntervalSince1970: 0)
    
    
    var body: some View {
        VStack(spacing: 50) {
            
            VStack {
                Text(textWritten)
                TextField("Entrez un texte", text: $textWritten, onEditingChanged: {
                    (change) in
                    print("Something changed")
                }){
                    print("Something commited")
                }
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(8.0)
                
            }
            
            
            SecureField("Texte sécurisé", text: $textSecure)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding(8.0)
            
            
            Toggle(isOn: $toggle) {
                Text((self.toggle) ? "J'aime les chats" : "J'aime pas les chats")
            }
            
            
            Stepper (value: $number, in: 1...10, step: 2) {
                Text("Le stepper indique : \(self.number)")
            }
            
            
            VStack {
                Text("Taux d'écoute : \(Int(self.percent))")
                Slider(value: $percent, in: 0...100)
                    .padding()
            }
            
            
            VStack {
                Text("Mon fruit préféré est : \(fruits[indexSelectedFruit])")
                Picker(selection: $indexSelectedFruit, label: Text("Fruits")) {
                    ForEach(0..<fruits.count, id:\.self) { fruit in
                        Text(self.fruits[fruit]).tag(fruit)
                    }
                }
            }
            
            DatePicker(selection: $date, in: dateMin...date, displayedComponents: .date) {
                Text("Choisir une date")
            }
            
        }
        .padding()
    }
}

struct FormView_Previews: PreviewProvider {
    static var previews: some View {
        FormView()
    }
}
