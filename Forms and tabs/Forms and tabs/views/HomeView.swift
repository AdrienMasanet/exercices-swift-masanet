//
//  HomeView.swift
//  Forms and tabs
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        Text("Page d'accueil")
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
