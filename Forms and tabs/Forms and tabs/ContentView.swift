//
//  ContentView.swift
//  Forms and tabs
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct ContentView: View {
    @State var selected = 0
    
    var body: some View {
        TabView(selection: $selected) {
            HomeView()
                .tabItem {
                    Image(systemName: "house.fill")
                    Text("Accueil")
                }
                .tag(0)
            
            FormView()
                .tabItem {
                    Image(systemName: "apps.ipad")
                    Text("Formulaire")
                }
                .tag(1)
            
            Text("Une tab sans view")
                .tabItem {
                    Image(systemName: "apps.ipad")
                    Text("Cheum")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
