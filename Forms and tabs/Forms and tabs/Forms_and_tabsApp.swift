//
//  Forms_and_tabsApp.swift
//  Forms and tabs
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

@main
struct Forms_and_tabsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
