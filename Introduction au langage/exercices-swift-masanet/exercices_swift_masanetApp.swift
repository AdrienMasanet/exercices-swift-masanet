//
//  exercices_swift_masanetApp.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

@main
struct exercices_swift_masanetApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
