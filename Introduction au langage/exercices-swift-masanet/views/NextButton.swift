//
//  NextButton.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

struct NextButton: View {
    var body: some View {
        Button (action: {
            print("Next button triggered")
        }){
            Text("Roll")
                .padding()
                .foregroundColor(Color.white)
                .background(Color.yellow)
                .cornerRadius(10)
                .font(.custom("Ok", size: 50))
                .shadow(radius: 1)
        }
    }
}

struct NextButton_Previews: PreviewProvider {
    static var previews: some View {
        NextButton()
    }
}
