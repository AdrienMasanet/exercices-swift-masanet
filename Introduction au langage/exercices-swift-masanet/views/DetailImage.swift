//
//  Image.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

struct DetailImage: View {
    var body: some View {
        Image("chat")
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(height: 325)
            .cornerRadius(10)
            .shadow(radius: 3)
    }
}

struct DetailImage_Previews: PreviewProvider {
    static var previews: some View {
        DetailImage()
    }
}
