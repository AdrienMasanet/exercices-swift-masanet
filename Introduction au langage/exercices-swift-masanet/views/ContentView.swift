//
//  ContentView.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

struct ContentView: View {
    @State var show = false
    
    var body: some View {
        NavigationView {
            VStack {
                Text("Rndomizer")
                    .padding()
                    .frame(maxWidth: .infinity)
                    .background(Color.black)
                    .foregroundColor(Color.white)
                    .font(.largeTitle)
                
                Spacer()
                
                Text("Moi aujourd'hui après une nuit très courte :")
                    .font(.custom("Chalkduster", size: 30))
                    .multilineTextAlignment(.center)
                
                DetailImage()
                
                Spacer()
                
                HStack {
                    NextButton()
                    NavigationLink("Aller sur la vue suivante", destination: AltView())
                }.padding()
                
                Spacer()
            }
            
            .navigationBarItems(leading: Button(action:{
                self.show.toggle()
            }){
                Text("Ouvrir sheetview").bold()
            }
                                , trailing: NavigationLink("Une autre vue", destination: AltView())
                .navigationBarTitle("Titre de la page")
                .sheet(isPresented: $show){
                    OptionalView()
                }
            )
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
