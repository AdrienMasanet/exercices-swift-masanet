//
//  AltView.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

struct AltView: View {
    var body: some View {
        VStack {
            Spacer()
            DetailImage()
            Spacer()
            Text("Vive les chats").padding()
            Spacer()
        }
    }
}

struct AltView_Previews: PreviewProvider {
    static var previews: some View {
        AltView()
    }
}
