//
//  SheetView.swift
//  exercices-swift-masanet
//
//  Created by Adrien Masanet on 07/06/2022.
//

import SwiftUI

struct OptionalView: View {
    var body: some View {
        Text("Cette vue est optionnelle")
    }
}

struct OptionalView_Previews: PreviewProvider {
    static var previews: some View {
        OptionalView()
    }
}
