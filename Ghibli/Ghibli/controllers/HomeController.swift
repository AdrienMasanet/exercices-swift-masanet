//
//  ContentView.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct HomeController: View {
    
    @ObservedObject var movieGetter = MovieGetter()
    
    var body: some View {
        NavigationView {
            List(movieGetter.movies, id: \.id) { movie in
                CellView(movie: movie)
            }
            
        }
        .navigationBarTitle("Ghibli or not Ghibli")
        .onAppear() {
            self.movieGetter.getFromApi()
        }
    }
}

struct HomeController_Previews: PreviewProvider {
    static var previews: some View {
        HomeController()
    }
}
