//
//  MovieDetail.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct MovieDetailController: View {
    
    var movie: MovieJsonResponse
    
    @ObservedObject var imageLoader = LoadImage()
    
    var body: some View {
        ScrollView {
            VStack {
                Text(movie.title)
                    .font(.largeTitle)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.brown)
                
                Text("Sorti en \(movie.release_date)")
                    .font(.title)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.orange)
                
                if imageLoader.image != nil {
                    Image(uiImage: imageLoader.image!)
                        .resizable()
                        .frame(width: 400, height: 400, alignment: .center)
                        .aspectRatio(contentMode: .fill)
                        .padding()
                }
                
                Text(movie.description)
                    .foregroundColor(.brown)
                    .padding(30)
            }
        }
        .onAppear() {
            imageLoader.load(movie.image)
        }
    }
}

/*
 struct MovieDetailController_Previews: PreviewProvider {
 static var previews: some View {
 MovieDetailController()
 }
 }
 */
