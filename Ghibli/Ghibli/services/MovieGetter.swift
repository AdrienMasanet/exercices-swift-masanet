//
//  MovieGetter.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI
import Combine

class MovieGetter: ObservableObject {
    var didChange = PassthroughSubject<[MovieJsonResponse], Never>()
    
    @Published var movies: [MovieJsonResponse] = [] {
        didSet {
            didChange.send(movies)
        }
    }
    
    func getFromApi() {
        
        let urlString = "https://ghibliapi.herokuapp.com/films"
        guard let url = URL(string: urlString) else {
            print("Error while parsing URL")
            return
        }
        
        print("Launching API request on url : \(url)")
        
        URLSession.shared.dataTask(with: url) { (d, _, e) in
            print("toto")
            if let error = e {
                print (error.localizedDescription)
            }
            
            if let data = d {
                do {
                    let response: [MovieJsonResponse] = try JSONDecoder().decode([MovieJsonResponse].self, from: data)
                    
                    print("MOVIES GATHERED :")
                    response.forEach{ (movie) in
                        print(movie.title)
                    }
                    
                    DispatchQueue.main.async {
                        self.movies = response
                    }
                } catch {
                    print(error)
                }
            }
        }
        .resume()
    }
    
}
