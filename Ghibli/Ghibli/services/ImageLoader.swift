//
//  ImageLoader.swift
//  Ghibli
//
//  Created by Adrien Masanet on 10/06/2022.
//

import SwiftUI
import Combine

class LoadImage: ObservableObject {
    var didChange = PassthroughSubject<UIImage?, Never>()
    
    @Published var image: UIImage? {
        didSet {
            didChange.send(image)
        }
    }
    
    func load(_ urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (d, _, _) in
            if let data = d {
                DispatchQueue.main.async {
                    self.image = UIImage(data: data)
                }
            }
        }
        .resume()
    }
}
