//
//  File.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import Foundation

struct MovieJsonResponse: Decodable {
    var id: String
    var title: String
    var image: String
    var description: String
    var director: String
    var producer: String
    var release_date: String
}
