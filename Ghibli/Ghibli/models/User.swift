//
//  UserJsonResponse.swift
//  Ghibli
//
//  Created by Adrien Masanet on 10/06/2022.
//

import Foundation

struct UsersJsonResponse: Decodable {
    var results: [UserJsonResponse]
}

struct UserJsonResponse: Decodable {
    var gender: String
    var name: UserNameJsonResponse
    
}

struct UserNameJsonResponse: Decodable {
    var title: String
    var first: String
    var last: String
}
