//
//  GhibliApp.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

@main
struct GhibliApp: App {
    var body: some Scene {
        WindowGroup {
            HomeController()
        }
    }
}
