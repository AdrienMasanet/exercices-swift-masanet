//
//  CellView.swift
//  Ghibli
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct CellView: View {
    
    var movie: MovieJsonResponse
    
    @ObservedObject var imageLoader = LoadImage()
    
    var body: some View {
        NavigationLink(destination: MovieDetailController(movie: movie)) {
            HStack {
                if imageLoader.image != nil {
                    Image(uiImage: imageLoader.image!)
                        .resizable()
                        .frame(width: 45, height: 45, alignment: .center)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.orange, lineWidth: 2))
                }
                
                Spacer()
                
                VStack(alignment: .trailing) {
                    Text(movie.title)
                        .font(.title3)
                        .foregroundColor(Color.brown)
                    Text("Année : \(movie.release_date)")
                        .font(.subheadline)
                        .italic()
                        .foregroundColor(Color.orange)
                }
            }
        }
        .onAppear() {
            imageLoader.load(movie.image)
        }
    }
}

/*
 struct CellView_Previews: PreviewProvider {
 static var previews: some View {
 CellView()
 }
 }
 */

