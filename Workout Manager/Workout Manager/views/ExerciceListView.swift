//
//  ExerciceListView.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct ExerciceListView: View {
    
    var exercice: Exercice
    
    let calendar = Calendar.current
    
    var startingTime: String {
        return "\(self.calendar.component(.hour, from: exercice.startingTime))h \(self.calendar.component(.minute, from: exercice.startingTime))m \(self.calendar.component(.second, from: exercice.startingTime))s"
    }
    
    var endingTime: String {
        return "\(self.calendar.component(.hour, from: exercice.endingTime))h \(self.calendar.component(.minute, from: exercice.endingTime))m \(self.calendar.component(.second, from: exercice.endingTime))s"
    }
    
    var body: some View {
        VStack{
            VStack {
                Text(exercice.name)
                    .font(.title)
                
                Image((exercice.outdoor) ? "outdoor" : "indoor")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(height: 125, alignment: .center)
                    .clipped()
                    .cornerRadius(10)
                    .shadow(radius: 5)
                
                Spacer()
                
                HStack {
                    Text("Début : \(startingTime)")
                    Spacer()
                    Text("Fin : \(endingTime)")
                }
                
                HStack {
                    Text("Répétitions : \(exercice.repetitions)")
                    Spacer()
                    Text("Difficulté : \(exercice.difficulty)")
                }
            }
            .padding()
            .frame(width: 350, height: 270, alignment: .center)
            .background(.thinMaterial)
        }
        .cornerRadius(10)
        .shadow(radius: 2)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(.gray, lineWidth: 1)
        )
    }
}

struct ExerciceListView_Previews: PreviewProvider {
    static var previews: some View {
        ExerciceListView(exercice: placeholderExercice)
    }
}
