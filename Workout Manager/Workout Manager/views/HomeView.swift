//
//  ContentView.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct HomeView: View {
    @State var showEditForm: Bool = false
    @State var exercices: [Exercice] = ExercicesCrud().getExercices()
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    VStack {
                        Button(action: {
                            self.showEditForm.toggle()
                        }){
                            Text("Ajouter un Exercice")
                                .padding()
                                .foregroundColor(.white)
                                .background(.primary)
                                .cornerRadius(100)
                                .shadow(radius: 2)
                                .font(.title)
                                .padding(25)
                        }
                        
                        ForEach(exercices) { exercice in
                            ExerciceListView(exercice: exercice)
                                .padding(.bottom)
                        }
                    }
                }
                .padding()
            }
            .navigationBarTitle("Workout Manager")
            .sheet(isPresented: $showEditForm) {
                ExerciceEditView(exercices: self.$exercices)
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
