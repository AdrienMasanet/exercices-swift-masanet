//
//  ExerciceEditView.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

struct ExerciceEditView: View {
    @Binding var exercices: [Exercice]
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var name: String = ""
    @State var outdoor: Bool = false
    @State var startingDate: Date = Date()
    @State var endingDate: Date = Date()
    @State var difficulty: CGFloat = 0
    @State var repetitions: Int = 10
    @State var placeholder = "Ajouter un exercice"
    
    var min: CGFloat = 0
    var max: CGFloat = 5
    
    var body: some View {
        VStack {
            Image("fitness")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(height: 250)
                .clipped()
            
            Form {
                
                Section(header: Text(placeholder)) {
                    TextField(placeholder, text: $name)
                    Toggle(isOn: $outdoor) {
                        Text((self.outdoor) ? "extérieur" : "intérieur")
                    }
                    DatePicker(selection: $startingDate){
                        Text("Début de l'entraînement")
                    }
                    DatePicker(selection: $endingDate, in: startingDate...Date(timeIntervalSinceNow: 36000)){
                        Text("Fin de l'entraînement")
                    }
                }
                
                Section(header: Text("Détail")) {
                    VStack {
                        HStack {
                            Text("Difficulté")
                            Spacer()
                            Text("\(Int(difficulty))")
                            
                        }
                        Slider (value: $difficulty, in: min...max)
                        Stepper("Nombre de répétitions \(repetitions)", value: $repetitions, in: 0...1000)
                    }
                }
            }
            
            Section(header: Text("Valider")) {
                Button(action: {
                    if self.name != "" {
                        let exercice = Exercice(name: self.name, outdoor: self.outdoor, difficulty: Int(self.difficulty), startingTime: self.startingDate, endingTime: self.endingDate, repetitions: self.repetitions)
                        
                        self.exercices.append(exercice)
                        self.presentationMode.wrappedValue.dismiss()
                    } else {
                        self.placeholder = "Nom d'exercice obligatoire"
                    }
                }){
                    Text("Valider l'exercice")
                }
            }
            
        }
    }
}


/*
 struct ExerciceEditView_Previews: PreviewProvider {
 static var previews: some View {
 ExerciceEditView()
 }
 }
 */
