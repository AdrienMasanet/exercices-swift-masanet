//
//  Workout_ManagerApp.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import SwiftUI

@main
struct Workout_ManagerApp: App {
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
}
