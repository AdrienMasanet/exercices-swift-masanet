//
//  ExerciceGetter.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import Foundation

var placeholderExercice: Exercice = Exercice(name: "Curl biceps", outdoor: false, difficulty: 5,startingTime: Date(), endingTime: Date(), repetitions: 10)

class ExercicesCrud {
    func getExercices() -> [Exercice] {
        let exercices: [Exercice] = [
            Exercice(name: "Curl biceps", outdoor: false, difficulty: 6, startingTime: Date(), endingTime: Date(), repetitions: 10),
            Exercice(name: "Burpees", outdoor: true, difficulty: 10, startingTime: Date(), endingTime: Date(), repetitions: 15),
            Exercice(name: "Squats sumo", outdoor: false, difficulty: 8, startingTime: Date(), endingTime: Date(), repetitions: 10),
            Exercice(name: "Squats", outdoor: false, difficulty: 9, startingTime: Date(), endingTime: Date(), repetitions: 10),
            Exercice(name: "Tractions", outdoor: true, difficulty: 8, startingTime: Date(), endingTime: Date(), repetitions: 8),
            Exercice(name: "Deadlift", outdoor: true, difficulty: 10, startingTime: Date(), endingTime: Date(), repetitions: 8),
            Exercice(name: "Jumping Jacks", outdoor: true, difficulty: 5, startingTime: Date(), endingTime: Date(), repetitions: 10),
            Exercice(name: "Crunchs", outdoor: false, difficulty: 3, startingTime: Date(), endingTime: Date(), repetitions: 20),
            Exercice(name: "Crunchs obliques", outdoor: false, difficulty: 2, startingTime: Date(), endingTime: Date(), repetitions: 12),
            Exercice(name: "Élévations latérales", outdoor: false, difficulty: 3, startingTime: Date(), endingTime: Date(), repetitions: 13)
        ]
        
        return exercices
    }
}
