//
//  Exercice.swift
//  Workout Manager
//
//  Created by Adrien Masanet on 09/06/2022.
//

import Foundation

struct Exercice: Identifiable {
    var id: UUID = UUID()
    var name: String
    var outdoor: Bool
    var difficulty: Int
    var startingTime: Date
    var endingTime: Date
    var repetitions: Int
}
