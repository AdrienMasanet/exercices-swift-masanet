//
//  demoListApp.swift
//  demoList
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

@main
struct demoListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
