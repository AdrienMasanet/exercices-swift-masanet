//
//  ContentView.swift
//  demoList
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct Car: Identifiable {
    var id: Int
    var model: String
}

struct ContentView: View {
    
    var languages = ["Anglais", "Allemand", "Espagnol", "Italien", "Russe"]
    
    var cars = [
        Car(id: 0, model: "2cv citroën"),
        Car(id: 1, model: "205 peugeot"),
        Car(id: 2, model: "Golf V volkswagen")
    ]
    
    var body: some View {
        VStack {
            List {
                Section(header: Text("Langage compilé")) {
                    HStack {
                        Text("Java")
                        Spacer()
                        Image(systemName: "chart.pie")
                    }
                    Text("Swift")
                    Text("Objective-C")
                }
                
                Section(header: Text("Langage interprété")) {
                    Text("Javascript")
                    Text("PHP")
                }
            }
            .listStyle(GroupedListStyle())
            
            /*
            List (languages, id: \.self) { language in
                Text(language)
            }
            
            List(cars, id: \.id) { car in
                HStack {
                    Text("\(car.id)")
                    Spacer()
                    Text(car.model)
                }
            }
            */
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
