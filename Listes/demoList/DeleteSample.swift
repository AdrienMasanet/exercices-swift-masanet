//
//  DeleteSample.swift
//  demoList
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct Language: Identifiable {
    var id: Int
    var name: String
}

struct DeleteSample: View {
    
    @State var languages = [
        Language(id: 0, name: "Swift"),
        Language(id: 1, name: "Objective-C"),
        Language(id: 2, name: "Javascript"),
        Language(id: 3, name: "Dart"),
        Language(id: 4, name: "Java"),
        Language(id: 5, name: "Kotlin")
    ]
    
    func delete (at offset: IndexSet) {
        if let first = offset.first {
            self.languages.remove(at: first)
        }
    }
    
    var body: some View {
        VStack {
            NavigationLink("Delete", destination: DeleteSample())
            List {
                ForEach(languages, id: \.id) { language in
                    Text(language.name)
                }.onDelete(perform: delete)
            }
        }
    }
}

struct DeleteSample_Previews: PreviewProvider {
    static var previews: some View {
        DeleteSample()
    }
}
