//
//  cityApp.swift
//  city
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

@main
struct cityApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
