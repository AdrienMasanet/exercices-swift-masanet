//
//  ContentView.swift
//  city
//
//  Created by Adrien Masanet on 08/06/2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var selectedCityIndex:Int = 1
    
    var currentCity: City {
        return getCities()[selectedCityIndex]
    }
    
    var body: some View {
        VStack {
            VStack{
                Image(currentCity.image)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 350, height: 350)
                    .clipShape(Circle())
                    .shadow(color: currentCity.gradient.last ?? .black, radius: 1, x: 2, y: 2)
                    .padding(.top, 45)
                Spacer()
                VStack (alignment: .leading, spacing: 10){
                    Text(currentCity.name)
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .fontWeight(.bold)
                    Text(currentCity.state)
                        .font(.title)
                        .foregroundColor(.white)
                        .italic()
                        .padding(.bottom, 20)
                    Text(currentCity.description)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                        .foregroundColor(.white)
                        .font(.headline)
                }
                .padding()
                Spacer()
                Button(action: {
                    let random = Int.random(in: 0..<getCities().count)
                    self.selectedCityIndex = random
                }){
                    Text("Une autre ville")
                        .font(.title)
                        .fontWeight(.bold)
                        .shadow(color: .black, radius: 1, x: 2, y: 2)
                }
                .padding(.bottom, 35)
            }
            .frame(maxWidth: .infinity)
            .background{
                LinearGradient(colors: currentCity.gradient, startPoint: .topLeading, endPoint: .bottom)
            }
        }
        .background(
            Image(currentCity.image)
        )
        .edgesIgnoringSafeArea(.all)
    }
    
    func getCities() -> [City] {
        var cities = [City]()
        
        let chicago = City(name: "Chicago", state: "Illinois", image: "chicago", gradient: [.clear, .blue], description: "Windy city, ou la ville venteuse. Chicago est une plus grande villes américaines. Elle est implantée au bord du lac Michigan, La Sear Tower fut la plus haute tour des USA jusqu'à 2013.")
        
        let dc = City(name: "Washington DC", state: "Washington DC", image: "dc", gradient: [.clear, .yellow], description: "Washington DC n'est pas la plus grande ville des Etats Unis mais en est la capitale. Elle y habite la maison blanche, le pentagone et nombre d'autres batiments d'Etat. Fun fact: aucun batiment ne peut être plus haut que le toit du capitole.")
        
        let frisco = City(name: "San Francisco", state: "Californie", image: "frisco", gradient: [.clear, .red], description: "Berceau des Hippies, de la tech et point d'entrée de la Silicon Valley. La diversité est le maître mot de San Francisco, la belle aux rues tortueuses et son Golden Gate. Sans oublier la fameuse prison d'Alcatraz.")
        
        let la = City(name: "Los Angeles", state: "Californie", image: "la", gradient: [.clear, .orange], description: "City Of Angels: La ville des anges. Lorsque l'on pense à Los Angeles, on voit les immenses plages de Long Beach, le signe Hollywood, les quartiers chics de Beverly Hills, le walk of fame de Hollywood Boulevard. Le rêve américain en somme.")
        
        let nyc = City(name: "New York City", state: "New York", image: "nyc", gradient: [.clear, .purple], description: "Big Apple, la statue de la liberté, la ville qui ne dort jamais. Quand on pense aux USA, l'image de New York est souvent celle qui nous vient. De la 5eme avenue au Bronx, cette ville saura vous enchanter.")
        
        cities.append(contentsOf: [chicago, dc, frisco, la, nyc])
        
        return cities
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
