//
//  City.swift
//  city
//
//  Created by Adrien Masanet on 08/06/2022.
//

import Foundation
import SwiftUI

struct City {
    var name: String
    var state: String
    var image: String
    var gradient: [Color]
    var description: String
}
